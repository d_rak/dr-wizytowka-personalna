# {{DR}} - Wizytówka personalna Dariusz Rak


## ℹ️ README ENG
[README in English language](README.md)

## 🚀 PREZENTACJA PROJEKTU (na żywo)
Adres do działającej aplikacja [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

## 🖥 Wizualizacja (zrzut ekranu)
<img src="assets/img/portfolio/dr_portfolio.webp" width="1000">

## 📝 Opis
Dzień dobry,

nazywam się Dariusz Rak i swoją ścieżkę rozwoju oraz pracy w świecie IT skierowałem głównie w stronę stania się Web Developerem a dokładniej Backend Developerem (PHP / Symfony).

Zaprezentuję tutaj stworzoną witrynę internetową typu OnePage przedstawiającą personalną wizytówkę mojej osoby. Dzięki której mam dodatkowe możliwości do prezentowania informacji o sobie (i to lepiej w jednym miejscu 😉). 

Zawarte dane na stronie internetowej zdecydowanie ułatwią poznanie mojej osoby rekruterom oraz osobom zainteresowanych współpracą. Śmiało można powiedzieć że stworzona witryna przypomina szeroko rozbudowane CV. 
 
<br>
<br>

Powyższy krótki opis jest małym wstępem do stworzonej personalnej wizytówki znajdującej się pod linkiem  [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

Zapraszam do odwiedzenia strony, a w przypadku zainteresowania współpracą do nawiązania kontaktu. 

Dziękuję za poświęcony czas i uwagę. 🙂 

## 🙋‍♂️ Kontakt
Kontakt  na stronie internetowej [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)




