var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})


const videos = document.querySelectorAll('video');
videos.forEach(vid => {
    vid.onplay = function () {
        vid.classList.remove('img-grayscale');
    };

    vid.onpause = function () {
        vid.classList.add('img-grayscale');
    };
});


const circles = document.querySelectorAll('.circle-color');
const radius = circles[0].r.baseVal.value;
const circumference = radius * 2 * Math.PI;

for (let x = 0; x < circles.length; x++) {
    circles[x].style.strokeDasharray = circumference + " " + circumference;
    circles[x].style.strokeDashoffset = circumference;
    setProgress(circles[x]);
};

function setProgress(circle) {
    const percent = (circle.dataset.time / circle.dataset.allTime) % 1;
    const offset = circumference - percent * circumference;
    circle.style.strokeDashoffset = offset;
}

