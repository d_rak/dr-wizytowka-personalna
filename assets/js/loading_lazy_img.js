const optionsObserver = {
    rootMargin: '0px 0px 100px 0px',
    threshold: 0
};

let observer = new IntersectionObserver(function (entries, self) {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            const img = entry.target;
            img.src = img.dataset.src;
            self.unobserve(entry.target);
        }
    });
}, optionsObserver);


const imgs = document.querySelectorAll('img[data-src]');
imgs.forEach(img => {
    observer.observe(img);
});


window.addEventListener('DOMContentLoaded', () => {
    if (window.matchMedia('print').matches) {
        imgs.forEach(img => {
            img.src = img.dataset.src;
        });
    };
});