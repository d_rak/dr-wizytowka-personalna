const preloader = document.querySelector('.preloader');
preloader.classList.add('loaded');
loadingPage();

window.addEventListener("load", () => {
    preloader.classList.add('hide');
    setTimeout(function () {
        preloader.classList.remove('loaded');
    }, 800);

});

function loadingPage() {
    const maxHeight=window.innerHeight;
    const maxWidth=window.innerWidth;
    const heightP3=maxHeight/3;
    const widthP3=maxWidth/3;
    let tag=['<script>','<html>','{{Twig}}','$CV','<?php','.CSS','git add','SELECT * FROM CV','$.ajax','$ symfony new CV','Bootstrap','Wordpress','JSON','composer update','doctrine:'];
    

    //PL wysokosc litery 36px (male litery 24px), szerokosc litery 15px (male litery 10px)
    //PL ukad 8 wyrazow + 1 wyraz - srodkowy jest pusty

    // height letters 36px (small letters 24px), width letters 15px (small letters 10px)
    // set 8 words + 1 word - the middle one is empty
    const heighChar = (maxWidth > 576 ? 36 : 24);
    const widthChar = (maxWidth > 576 ? 15 : 10);

    let i = 0;
    tag = tag.sort(() => Math.random() - 0.5)
    for (let y = 0; y < 3; y++) {
        for (let x = 0; x < 3; x++) {
            if (x == 1 && y == 1) 
                continue;
            
            let mark = document.createElement("span");
            let node = document.createTextNode(tag[i]);
            mark.appendChild(node);
            positonTop = Math.floor(heighChar + Math.random() * (heightP3 - heighChar * 2) + heightP3 * y) + "px";
            mark.style.top = positonTop;
            positonLeft = Math.floor(widthChar + Math.random() * (widthP3 -(widthChar + 1) * tag[i].length) + widthP3 * x) + "px";
            i++;
            mark.style.left = positonLeft;
            preloader.appendChild(mark);
        }
    }
};