const btnTheme = document.querySelector('#ThemeIcon input');


window.addEventListener("load", () => {
    let Theme = document.cookie.split('Theme=')[1];
    if (Theme == true || window.matchMedia("(prefers-color-scheme: dark)").matches == true) {
        document.querySelector('body').classList.add("dark");
        btnTheme.checked = true;
    } else if (window.matchMedia("(prefers-color-scheme: dark)").matches == false) {
        document.querySelector('body').classList.remove("dark");
        btnTheme.checked = false;
    }
});


btnTheme.addEventListener("click", () => {
    if (btnTheme.checked == true) {
        document.cookie = "Theme=1"     // dark
    } else {
        document.cookie = "Theme=0"     // light
    }
    document.querySelector('body').classList.toggle("dark");
});

