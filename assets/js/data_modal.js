const btnContact = document.querySelector('#btnContact');
const myModalEl = document.querySelector('#modalContact');

export const dataContact = [
    "(+48) 883 451 912", 
    "DariuszRak@itsparta.pl",
    `Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do nawiązania kontaku oraz do realizacji procesu rekrutacji na stanowisko 
    pracy (zgodniez ustawą z dnia 10 maja 2018 roku o&nbsp;ochronie danych osobowych (Dz. Ustaw z 2018, poz. 1000) oraz zgodnie z&nbsp;Rozporządzeniem Parlamentu 
    Europejskiego i&nbsp;Rady (UE) 2016/679 z&nbsp;dnia 27 kwietnia 2016 r. w&nbsp;sprawie ochrony osób fizycznych w&nbsp;związku z&nbsp;przetwarzaniem danych osobowych 
    i&nbsp;w&nbsp;sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (RODO)`];


btnContact.addEventListener("click", () => {
    btnContact.classList.add('active');
    document.querySelector('#modalContact .telField span').innerHTML=dataContact[0];
    document.querySelector('#modalContact .emailField span').innerHTML=dataContact[1];
    document.querySelector('#modalContact .telField').href="tel:"+dataContact[0];
    document.querySelector('#modalContact .emailField').href="mailto:"+dataContact[1];
    document.querySelector('#modalContact .consentDataProcessingField').innerHTML=dataContact[2];
});

myModalEl.addEventListener('hidden.bs.modal', () => {
    btnContact.classList.remove('active');
})
