const navbarNavCollapse = document.querySelector('#navbar-nav-box');

const navbarNav = new bootstrap.Collapse(navbarNavCollapse, {toggle: false})


const linkNav = navbarNavCollapse.querySelectorAll('a.nav-link');
const btnNav = document.querySelector('#btnMenu');


window.addEventListener("load", menuAutoHide);
window.addEventListener("resize", menuAutoHide);


function menuAutoHide() {
    if (window.innerWidth < 992) {
        for (var x = 0; x < linkNav.length; x++) {
            linkNav[x].addEventListener("click", e => {
                if (btnNav.getAttribute("aria-expanded") == 'true' && window.getComputedStyle(btnNav).display != 'none') {
                    navbarNav.hide();
                    let posX = document.querySelector(e.currentTarget.getAttribute('href')).offsetTop - 80;
                    setTimeout(() => {
                        window.scrollTo({top: posX, behavior: 'smooth'});
                    }, 850);
                }
            });
        }
    }
}

