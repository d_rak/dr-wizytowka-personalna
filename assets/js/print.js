import {dataContact} from './data_modal.js';

const btnPrintIcon = document.querySelector('#btnPrintIcon');
const mainPrint = document.querySelector('#main');
const accordionsPrint = document.querySelectorAll('div.accordion-item');
const videoBoxPrint = document.querySelectorAll('video');
const SkillsBoxes = document.querySelectorAll('.row-cols-1.row-cols-md-2');
const links = document.querySelectorAll('#gitlabLink, #linkedinbLink');
const personContact = document.querySelector('#personContact');


btnPrintIcon.addEventListener("click", () => {
    window.print();
});

// for test to print
window.addEventListener('DOMContentLoaded', () => {
    if (window.matchMedia('print').matches) {
        prepareToPrint();
    }
});

window.addEventListener('beforeprint', prepareToPrint);
window.addEventListener('afterprint', prepareToAfterPrint);


function prepareToPrint() {
    mainPrint.classList.remove('col-lg-7');

    // Show attr href in links
    links.forEach(link => {
        let hrefAttr = document.createElement("span");
        hrefAttr.innerHTML = link.getAttribute('href').split('https://')[1];
        hrefAttr.classList.add('small');
        hrefAttr.classList.add('d-block');
        link.insertAdjacentElement('beforeend', hrefAttr);
    });

    // Edit contact data
    document.querySelector('#btnContact').parentElement.classList.add('d-none');
    personContact.classList.remove('d-none');
    personContact.querySelector('.consentDataProcessingField').innerHTML = dataContact[2];
    personContact.querySelector('.telField span').innerHTML = dataContact[0];
    personContact.querySelector('.emailField span').innerHTML = dataContact[1];


    // remove lazy_loading
    const imgsPrint = document.querySelectorAll('img[data-src]');
    imgsPrint.forEach(img => {
        img.src = img.dataset.src;
    });

    // show all accordion
    accordionsPrint.forEach(accordion => {
        accordion.querySelector('.accordion-collapse').classList.add('show');
    });

    // add adres URL webpage
    let pUrlWeb = document.querySelector("p#urlWeb");
    if (! pUrlWeb) {
        const dr_logo = document.querySelector('#dr_logo');
        pUrlWeb = document.createElement("p");
        pUrlWeb.setAttribute("id", "urlWeb");
        let url = window.location.href.split('#')[0];
        url = url.split('https://')[1];
        if (url) {
            pUrlWeb.innerHTML = "WEBSITE: " + url;
            dr_logo.after(pUrlWeb);
        }
    }

    // show all poster image for video
    videoBoxPrint.forEach(videoElement => {
        let srcAttr = videoElement.getAttribute('poster');
        videoElement.insertAdjacentHTML('beforebegin', '<img class="mx-auto d-block img-fluid img-only-print" src="' + srcAttr + '" alt="">');
    });

    // Add all skills boxes class for print
    SkillsBoxes.forEach(SkillsBox => {
        SkillsBox.classList.remove('row-cols-1');
        SkillsBox.classList.add('row-cols-2');
    });

    // add page break for print
    const allPageBreak = document.querySelectorAll('#fixed-box, #profil, #accordionWork div.accordion-item:nth-of-type(2), #doswiadczenie, #softSkills, #ITSkills, #umiejetnosci, #portfolio .col-10 div.row:nth-of-type(1), #portfolio .col-10 div.row:nth-of-type(2), #portfolio .col-10 div.row:nth-of-type(3), #portfolio');
    for (let counterPB = 0; counterPB < allPageBreak.length; counterPB++) {
        const divPB = document.createElement("div");
        divPB.classList.add('page-break-after');
        allPageBreak[counterPB].insertAdjacentElement('afterend', divPB);
    }

}


function prepareToAfterPrint() {
    mainPrint.classList.add('col-lg-7');

    // Show attr href in links
    links.forEach(link => {
        let hrefAttr = link.querySelector('span');
        hrefAttr.remove();
    });

    // Edit contact data
    document.querySelector('#btnContact').parentElement.classList.remove('d-none');
    personContact.classList.add('d-none');
    document.querySelector('#personContact .telField span').innerHTML = '';
    document.querySelector('#personContact .emailField span').innerHTML = '';


    // hide all accordion
    accordionsPrint.forEach(accordion => {
        accordion.querySelector('.accordion-collapse').classList.remove('show');
    });

    // remove adres URL webpage
    let pUrlWeb = document.querySelector('p#urlWeb');
    if (pUrlWeb) {
        pUrlWeb.remove();
    }

    // remove all poster image for video
    let imgsOnlyPrint = document.querySelectorAll(".img-only-print");
    if (imgsOnlyPrint) {
        imgsOnlyPrint.forEach(imgOnlyPrint => {
            imgOnlyPrint.remove();
        });
    }

    // Remove all skills boxes class for print
    SkillsBoxes.forEach(SkillsBox => {
        SkillsBox.classList.remove('row-cols-2');
        SkillsBox.classList.add('row-cols-1');
    });

    // remove page break for print
    const allPageBreak = document.querySelectorAll('div.page-break-after');
    for (let counterPB = 0; counterPB < allPageBreak.length; counterPB++) {
        allPageBreak[counterPB].remove();
    }

}
