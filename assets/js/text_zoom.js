let boxesText = document.querySelectorAll('#main section');
let navItem = document.querySelectorAll('#navbar-box ul li a');
let boxPersonTopPT;
let boxPersonHeightPB;

window.addEventListener("resize", () => {
    boxPersonTopPT = parseInt(document.querySelector('#person').offsetTop + 35);
    boxPersonHeightPB = parseInt(document.querySelector('#person').offsetHeight - 35);
    boxZoom();
});


window.addEventListener("scroll", boxZoom);

function boxZoom() {
    let boxPersonTopPT = parseInt(document.querySelector('#person').offsetTop + 35);
    let boxPersonHeightPB = parseInt(document.querySelector('#person').offsetHeight - 35);
    let boxPersonHalfHeightHH = parseInt(boxPersonTopPT + (boxPersonHeightPB) / 2);
    let currentScroll = parseInt(window.pageYOffset);
    
    for (let x = 0; x < boxesText.length; x++) {
        if (currentScroll + boxPersonTopPT < parseInt(boxesText[x].offsetHeight + boxesText[x].offsetTop) && currentScroll + boxPersonHeightPB > parseInt(boxesText[x].offsetTop)) {
            if (boxesText[x].classList.contains("size80P")) {
                boxesText[x].classList.add('size100P');
                boxesText[x].classList.remove('size80P');
            }
        } else {
            if (boxesText[x].classList.contains("size100P")) {
                boxesText[x].classList.add('size80P');
                boxesText[x].classList.remove('size100P');
                navItem[x].classList.remove('active');
            }
        }
        if (currentScroll + boxPersonHalfHeightHH > boxesText[x].offsetTop && currentScroll + boxPersonHalfHeightHH < parseInt(boxesText[x].offsetHeight + boxesText[x].offsetTop) && boxesText[x].classList.contains("size100P")) {
            navItem[x].classList.add('active');
        }
    }
}

