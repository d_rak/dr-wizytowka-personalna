const textTyping = document.querySelector('#textWrite');

window.addEventListener("resize", movetextTyping);
window.addEventListener("load", typeWriter);

function movetextTyping() {
    let intFrameWidth = window.innerWidth;
    textTypingBox = document.querySelector('#textTyping section');
    textTypingMobile = document.querySelector('#textTypingMobile section');
    if(intFrameWidth>=992 && textTypingBox==null){
        textTypingBox = document.querySelector('#textTyping');
        textTypingBox.appendChild(document.querySelector('#textTypingMobile section'));
    }
    else if(intFrameWidth<992 && textTypingMobile==null){
        textTypingMobile = document.querySelector('#textTypingMobile');
        textTypingMobile.appendChild(document.querySelector('#textTyping section'));
    }
}

let txt = [ ['Dzień dobry ', 0],
            ['Jestem {{DR}} ',7],        // the second position means until when to delete
            ['Dariusz Rak ', 7],         // 7 characters, leave the set of words from the previous one, i.e. the word I am
            ['Web Developerem ',0],
            ['Zapraszam do wizytówki.'] ]; 
let speed = 120;                        // the speed/duration of the effect in milliseconds

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function typeWriter() {
    movetextTyping();
    textTyping.innerHTML = '';
    textTyping.classList.add('pulse');
    await sleep(1000);
    textTyping.classList.remove('pulse');
    await sleep(100);

    for (let x = 0; x < txt.length; x++) {
        for (let i = 0; i < txt[x][0].length; i++) {
            textTyping.innerHTML += txt[x][0].charAt(i);
            await sleep(speed);
        }
        textTyping.classList.add('pulse');
        await sleep(1000);
        textTyping.classList.remove('pulse');
        await sleep(100);

        do {
            textTyping.innerHTML = textTyping.innerHTML.slice(0, textTyping.innerHTML.length - 1);
            await sleep(speed);
            if (txt[x][1] != 0) {
                if (textTyping.innerHTML.length == txt[x][1]) 
                    break;
                
            }
        } while (textTyping.innerHTML != '');
        textTyping.classList.add('pulse');
        await sleep(1000);
        textTyping.classList.remove('pulse');
        await sleep(100);
    }
    return typeWriter();
}

