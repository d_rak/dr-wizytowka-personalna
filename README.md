# {{DR}} - Personal card Dariusz Rak

## ℹ️ README PL
[README w języku polskim](README_PL.md)

## 🚀 PROJECT PRESENTATION (live)

Adress to live application [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

## 🖥 Visuals (screen)
<img src="assets/img/portfolio/dr_portfolio.webp" width="1000">

## 📝 Description
Hello,

my name is Dariusz Rak and my path of development and work in the IT world was directed mainly towards becoming a Web Developer, and more specifically a Backend Developer (PHP / Symfony).

I will present here a created OnePage website presenting my personal business card. Thanks to which I have additional opportunities to present information about myself (and it's better in one place 😉). 

The data contained on the website will definitely make it easier for recruiters and people interested in cooperation to get to know me. It is safe to say that the created website resembles a broadly developed CV.
 
<br>
<br>

The above short description is a small introduction to the created personal business card located at the link [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

I invite you to visit the website, and if you are interested in cooperation, please contact me.

Thank you for your time and attention. 🙂



## 🙋‍♂️ Contact
Contact on the website [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

